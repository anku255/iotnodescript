const axios = require("axios");
const fs = require("fs");
const FormData = require("form-data");
const concat = require("concat-stream");

const pathToCSVFile = "./58168.csv";
const SERVER_URL = "http://40.121.56.106:3090/api/csv";

const promise = new Promise(resolve => {
  const fd = new FormData();
  fd.append("file", fs.createReadStream(pathToCSVFile));
  fd.pipe(
    concat({ encoding: "buffer" }, data =>
      resolve({ data, headers: fd.getHeaders() })
    ) 
  );
});

promise.then(({ data, headers }) =>
  axios
    .post(SERVER_URL, data, { headers })
    .then(res => {
      console.log("response", res.data);
    })
    .catch(err => console.log("Error Occurred!", err))
);
